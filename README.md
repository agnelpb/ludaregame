# Ludare 2D Platformer Prototype #

This repo includes the TDD and the prototype implementation.

### Getting Started ###

* Read the TDD for breakdown of the project.
* Download / Clone the repo.
* Open the project in Unity 2020.3.19f1 LTS Version .
* Open the Scenes / MainMenu
* Play on Editor.


### Modifying Values ###

The data folder contains various Data scriptable objects for character, collectables and enemy.

Any new EnemyType or CollectableType will have to be added by the devs, but once it is added, you can modify the values in these scriptable Objects.


### Adding New Content ###

#### Adding New Level ####

To add a new level, the easiest way is to duplicate Level1 Scene and modifying the contents in it.

To add a new level from scratch, make sure the scene has the following prefabs in it :

* Character
* EventSystem
* SpawnManager
* Camera
* Cinemachine 2D Camera
* LevelManager
* UI Manager

To test a level without having to go through previous level, you will also have to add :

* GameController
* PoolManager
* DataManager

However, these can be removed from the level for final release as they persist across scene.

#### Designing the Level ####

In order to design a new level, you can use prefabs such as

* Floor
* Danger

The Parallax background needs a "Parallax" script attached to it and the speed can be set with the parallax speed in relation to user input.

To add new collectables, add a gameobject with the component "SpawnPoint" and select the Collectable prefab you want to spawn.
To add new enemies, add a gameobject with the component "SpawnPoint" and select the enemy prefab you want to spawn.
For enemies, you will have to also add the prefab "EnemyBoundary" to make the bounds in which the enemy will move.

Note : Whenever a SpawnPoint is added, it will automatically be registered to the SpawnManager. So ensure you have added a SpawnManager before you start adding SpawnPoints.

To add the level end object, you can use the prefab "LevelEndObject".

Currently the game has only one collectable and one enemy type. However it is very easy to add new types if the devs desire it. From then on, the designers and artists can
create new prefabs and use the drop down menu to select the type. You will also need to update the Data folder when new types are added.

Once the scene is ready, Add it to the Build Settings.

In LevelManager of the previous scene, you can specify the new scene name as value for *NextLevel*. So when a level is complete, it will load your new level as the next level.

#### Modifying Input ####

The Input Keys can be modified by going to Edit > ProjectSettings > InputManager
Change the values for Horizontal and Jump to set alternate input keys.

#### Building Prototype ####

* Go to File > Build Settings > Select PC Standalone
* Click on Build and save it with the name you desire.
* Open the .exe file to play the prototype.

### Assets Used ###

* CineMachine
* Milla Cilla by Typhoon Type™ - Suthi Srisopha (Font)
* TextMesh Pro
* Pooling System by Agnel Blaise

### Foot Notes ###

The prototype implements a Pooling system written by me. So if you want to optimize game performance, you can add a "PoolManager" to MainMenu scene and specify which prefabs you
would like to spawn ahead of time to reduce instantiation during runtime.

Due to limited timeframe, this is the best I could do for a prototype. 2D platformers are notorious for the effort it takes to get the character movement to feel good. However, that is beyond the scope of a prototype. In real world scenario, I would assign a day or two at least for research into various 2D platformers to get the character controls to feel more tight and predictable.

This is all prototype code. In a real project scenario, none of this code will be used for production. It will all be revamped for better architecture to support easier
level design and easier extensibility.
