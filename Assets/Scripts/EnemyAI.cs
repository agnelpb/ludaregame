using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyAI : MonoBehaviour, ISpawnedInstance
{
    public enum EnemyType
    {
        Hexagon
    }

    public EnemyType enemyType;

    private Vector3 movementDirection = Vector3.zero;

    private EnemyData.EnemyTypeData enemyData; 

    private bool isAlive = false;

    [SerializeField] private LayerMask layerMask;


    private SpawnPoint spawnPoint;

    private void Awake()
    {
        movementDirection = Random.Range(0, 1) <= 0.5f ? Vector3.right : Vector3.left;

        StartCoroutine(WaitForServices());
    }

    public IEnumerator WaitForServices()
    {
        WaitForEndOfFrame frameWait = new WaitForEndOfFrame();
        while (GameController.Instance.DataManager == null)
        {
            yield return frameWait;
        }

        enemyData = GameController.Instance.DataManager.EnemyData.GetData(enemyType);
        isAlive = true;
    }

    private void Update()
    {
        if (!isAlive) return;
        transform.Translate(movementDirection * (enemyData.Speed * Time.deltaTime ) );
    }

    private bool CalculatePlayerCollision(Collision2D collision)
    {
        Debug.Log("Character Collision with Enemy AI");
        ContactPoint2D[] contactPoints = new ContactPoint2D[1];
        collision.GetContacts(contactPoints);

        Debug.Log("Contact Point Y : " + contactPoints[0].point.y + ", Transform Point : " + transform.position.y);
        if (contactPoints[0].point.y > transform.position.y)
        {
            GameController.Instance.UIManager.IncrementEnemyScoreCounter();
            spawnPoint.Despawn();
            spawnPoint = null;
        }
        else
        {
            collision.transform.GetComponent<CharacterController>().Kill();
        }

        return true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "EnemyBoundary")
        {
            movementDirection = (movementDirection == Vector3.right) ? Vector3.left : Vector3.right;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {   
        if (collision.transform.tag == "Character")
        {
            CalculatePlayerCollision(collision);
        }
    }

    public void SetSpawnPoint(SpawnPoint spawn)
    {
        spawnPoint = spawn;
    }
}

