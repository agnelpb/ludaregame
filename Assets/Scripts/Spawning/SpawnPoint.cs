using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Collectables.Collectable;
using static EnemyAI;
using static SpawnManager;

public class SpawnPoint : MonoBehaviour, ISpawnable
{
    public GameObject PrefabToSpawn;

    private GameObject spawnedInstance = null;
    private PoolManager poolManager = null;

    private void OnValidate()
    {
        var SpawnManager = GameObject.FindObjectOfType<SpawnManager>();
        SpawnManager.RegisterToSpawnManager(this);
    }

    // Start is called before the first frame update
    void Awake()
    {
        Spawn();
    }

    void OnDestroy()
    {
        Despawn();
    }

    public void Spawn()
    {
        if (poolManager == null)
        {
            StartCoroutine(WaitForServices());
        }
        else
        {

            if (spawnedInstance != null) { return; }

            Debug.Log("Spawning");
            spawnedInstance = poolManager.GetFromPool(PrefabToSpawn);            
            spawnedInstance.SetActive(true);
            spawnedInstance.transform.position = transform.position;
            spawnedInstance.GetComponent<ISpawnedInstance>().SetSpawnPoint(this);
        }
    }

    public void Despawn()
    {
        if (spawnedInstance != null)
        {
            spawnedInstance.SetActive(false);
            poolManager.ReturnToPool(PrefabToSpawn, spawnedInstance);
            spawnedInstance = null;
        }
    }

    public IEnumerator WaitForServices()
    {   
        WaitForEndOfFrame frameWait = new WaitForEndOfFrame();
        while (GameController.Instance.PoolManager == null)
        {
            yield return frameWait;
                
        }
        poolManager = GameController.Instance.PoolManager;

        Spawn();
        yield return null;
    }
}
