using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public List<ISpawnable> SpawnableList = new List<ISpawnable>();

    private void Awake()
    {
        GameController.Instance.SpawnManager = this;

        // Sanitation
        for (int i = 0; i < SpawnableList.Count; i++)
        {
            if (SpawnableList[i] == null)
            {
                SpawnableList.RemoveAt(i);
            }
        }
    }
    
    public void RegisterToSpawnManager(ISpawnable spawnable)
    {
        if (SpawnableList.Contains(spawnable)) { return; }
        SpawnableList.Add(spawnable);
    }

    public void SpawnAll()
    {
        foreach (var spawnable in SpawnableList)
        {   
            spawnable?.Spawn();
        }
    }

    public void DespawnAll()
    {
        foreach (var spawnable in SpawnableList)
        {
            spawnable?.Despawn();
        }
    }

    [ContextMenu("Debug")]
    private void DebugSize()
    {
        Debug.Log("ISpawnables in Scene : " + SpawnableList.Count);
    }

    
    [ContextMenu("Clear. Dev Only.")]
    private void ClearSpawnables()
    {
        SpawnableList.Clear();
    }
}
