using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnedInstance
{
    public void SetSpawnPoint(SpawnPoint spawnPoint);
}
