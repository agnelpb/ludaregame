using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float Speed;

    void Update()
    {
        var movementDirection = Input.GetAxis("Horizontal");
        transform.Translate(-movementDirection * Vector2.right * Speed);
    }
}
