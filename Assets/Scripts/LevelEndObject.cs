using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelEndObject : MonoBehaviour
{
    [SerializeField] private UnityEvent OnLevelEnd;

    public void EndLevel()
    {
        OnLevelEnd?.Invoke();
    }
}
