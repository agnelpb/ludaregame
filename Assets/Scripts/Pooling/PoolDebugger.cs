﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Monobehaviour script to expose the Pool Size and List
/// </summary>
public class PoolDebugger : MonoBehaviour
{
    public Pool<GameObject> Pool;

    public int PoolSize;
    public List<GameObject> AvailablePoolObjects;
    public List<GameObject> UnavailablePoolObjects;

    void Start()
    {
        // Destroy the component during runtime.
        if (!Application.isEditor)
        {
            Destroy(this);
        }

        if (Pool != null)
        {
            Pool.OnPoolObjectsUpdated += OnPoolObjectsUpdated;
            Pool.OnPoolSizeChanged += OnPoolSizeChanged;

            OnPoolObjectsUpdated();
            OnPoolSizeChanged();
        }
    }

    private void OnDestroy()
    {
        if (Pool != null)
        {
            Pool.OnPoolObjectsUpdated -= OnPoolObjectsUpdated;
            Pool.OnPoolSizeChanged -= OnPoolSizeChanged;
        }
    }

    private void OnPoolObjectsUpdated()
    {
        AvailablePoolObjects = Pool.AvailablePoolObjects;
        UnavailablePoolObjects = Pool.UnavailablePoolObjects;
    }

    private void OnPoolSizeChanged()
    {
        PoolSize = Pool.PoolSize;
    }
}
