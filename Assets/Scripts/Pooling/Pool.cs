﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generic Pool Container
/// </summary>
/// <typeparam name="T"></typeparam>
public class Pool<T> where T : new() 
{
    // Maintaining multiple lists because some objects could be out of pool and also diabled in hierarchy during runtime.
    // Also helps with any advanced pool functionality in the future.
    protected List<T> availablePoolObjects = new List<T>();
    public List<T> AvailablePoolObjects{ get { return availablePoolObjects; } }

    protected List<T> unavailablePoolObjects = new List<T>();

    public List<T> UnavailablePoolObjects{ get { return unavailablePoolObjects; } }

    protected T prefab;

    protected int poolSize = 0;
    public int PoolSize { get { return poolSize; } }

    protected Transform parent;

    public Action OnPoolSizeChanged;
    public Action OnPoolObjectsUpdated;

    /// <summary>
    /// Pool Constructor 
    /// </summary>
    public Pool(T poolPrefab, int poolSize, Transform transformParent)
    {
        prefab = poolPrefab;
        this.poolSize = poolSize;
        parent = transformParent;
        AddToPool(this.poolSize);       
    }

    public virtual T GetPoolObject()
    {
        if (availablePoolObjects.Count == 0)
        {
            AddToPool(1);
            OnPoolSizeChanged?.Invoke();
        }

        var poolObject = availablePoolObjects[0];
        availablePoolObjects.Remove(poolObject);
        unavailablePoolObjects.Add(poolObject);
        OnPoolObjectsUpdated?.Invoke();

       

        return poolObject;                
    }

    public void ReturnObject(T poolObject)
    {
        if (availablePoolObjects.Count >= poolSize)
        {
            return;    
        }

        ReturnToPool(poolObject);
    }

    public void UpdatePoolSize(int newPoolSize)
    {
        var delta = newPoolSize - poolSize;        
        if (delta > 0) 
        {
            AddToPool(delta);            
        }
        else
        {
            RemoveFromPool(Mathf.Abs(delta));
        }
        poolSize = newPoolSize;
        OnPoolSizeChanged?.Invoke();
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="count"></param>
    protected virtual void AddToPool(int count) 
    {
        for (int i = 0; i < count; i++)
        {
            if (prefab is GameObject)
            {
                var poolGameObject = GameObject.Instantiate(prefab as GameObject);
                poolGameObject.SetActive(false);
                poolGameObject.transform.parent = parent;
                (availablePoolObjects as List<GameObject>).Add(poolGameObject);
            }
            else if (prefab is Component)
            {   
                var poolGameObject = GameObject.Instantiate(new GameObject(typeof(T).ToString()));
                poolGameObject.AddComponent(typeof(T));
                poolGameObject.SetActive(false);
                poolGameObject.transform.parent = parent;
                availablePoolObjects.Add(poolGameObject.GetComponent<T>());
            }
            else
            {
                var poolObject = new T();
                availablePoolObjects.Add(poolObject);
            }
        }

        poolSize = availablePoolObjects.Count + unavailablePoolObjects.Count;
        OnPoolSizeChanged?.Invoke();
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="poolObject"></param>
    protected virtual void ReturnToPool(T poolObject)
    {
        if (poolObject is GameObject)
        {
            (poolObject as GameObject).SetActive(false);
        }
        unavailablePoolObjects.Remove(poolObject);
        availablePoolObjects.Add(poolObject);
        OnPoolObjectsUpdated?.Invoke();
    }

    private void RemoveFromPool(int count)
    {
        for (int i = 0; i < count; i++)
        {
            if (availablePoolObjects.Count > 0)
            {
                availablePoolObjects.RemoveAt(0);
            }
        }        
    }
}
