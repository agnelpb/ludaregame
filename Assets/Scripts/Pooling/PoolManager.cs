﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pool Manager 
/// </summary>
public class PoolManager : MonoBehaviour
{
    [System.Serializable]
    public struct DefaultPoolPrefabs
    {
        public GameObject Prefab;
        public int PoolSize;
    }

    public List<DefaultPoolPrefabs> PoolPrefabs;

    private Dictionary<GameObject, Pool<GameObject>> objectPoolLookup = new Dictionary<GameObject, Pool<GameObject>>();
    private Dictionary<string, Pool<Object>> genericPoolLookup = new Dictionary<string, Pool<Object>>();

    public void Awake()
    {
        if (GameController.Instance.PoolManager != null)
        {
            Destroy(gameObject);
            return;
        }

        GameController.Instance.PoolManager = this;
        DontDestroyOnLoad(gameObject);

        foreach (var poolPrefab in PoolPrefabs)
        {
            CreatePool(poolPrefab.Prefab, poolPrefab.PoolSize);
        }
    }

    public Pool<GameObject> CreatePool(GameObject prefab, int poolSize)
    {
        var poolParent = new GameObject(prefab.name + "Pool");
        poolParent.transform.parent = transform;

        var pool = new Pool<GameObject>(prefab, poolSize, poolParent.transform);
        objectPoolLookup.Add(prefab, pool);
        poolParent.AddComponent<PoolDebugger>().Pool = pool;
        return pool;
    }

    public Pool<T> CreatePool<T>(string name, int poolSize) where T : new()
    {
        var poolParent = new GameObject(name + "Pool");
        poolParent.transform.parent = transform;

        var pool = new Pool<T>(new T(), poolSize, poolParent.transform);
        genericPoolLookup.Add(name, pool as Pool<Object>);
        return pool;
    }

    public Pool<GameObject> GetPool(GameObject prefab)
    {
        if (!objectPoolLookup.ContainsKey(prefab))
        {
            CreatePool(prefab, 1);
        }

        return objectPoolLookup[prefab];
    }

    public Pool<T> GetPool<T>(string name) where T : new()
    {
        if (!genericPoolLookup.ContainsKey(name))
        {
            CreatePool<T>(name, 1);
        }

        return genericPoolLookup[name] as Pool<T>;
    }

    public GameObject GetFromPool(GameObject prefab)
    {
        var pool = GetPool(prefab);
        var poolObject = pool.GetPoolObject();
        return poolObject;
    }

    public T GetFromPool<T>(string name) where T : new()
    {
        Debug.Log("Getting from Pool Name " + name);
        var pool = GetPool<T>(name);
        if (pool == null) { Debug.Log("Pool is null"); }
        return pool.GetPoolObject();
    }

    public void ReturnToPool(GameObject prefab, GameObject poolObject)
    {
        var pool = GetPool(prefab);
        pool.ReturnObject(poolObject);
    }

    public void ReturnToPool<T>(string name, T poolObject) where T : new()
    {
        var pool = GetPool<T>(name);
        pool.ReturnObject(poolObject);
    }
}
