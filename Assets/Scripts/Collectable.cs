using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class Collectable : MonoBehaviour, ISpawnedInstance
    {
        public Collider2D Collider;

        private SpawnPoint spawnPoint;

        public enum CollectableType
        {
            Diamond
        }

        public CollectableType Type;

        public void OnCollected()
        {
            spawnPoint.Despawn();
            spawnPoint = null;
        }

        public void SetSpawnPoint(SpawnPoint spawn)
        {
            spawnPoint = spawn;
        }
    }
}
