using Collectables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public CharacterData CharacterData;
    public CollectableData CollectableData;
    public EnemyData EnemyData;

    private void Awake()
    {
        if (GameController.Instance.DataManager != null) 
        {
            Destroy(gameObject);
            return;
        }

        GameController.Instance.DataManager = this;
        DontDestroyOnLoad(gameObject);
    }
}
