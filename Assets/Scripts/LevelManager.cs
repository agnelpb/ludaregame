using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public string NextLevel;
    public string PreviousLevel;

    private void Awake()
    {
        GameController.Instance.LevelManager = this;
    }

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void LoadNextLevel()
    {
        LoadLevel(NextLevel);
    }

    public void LoadPreviousLevel()
    {
        LoadLevel(PreviousLevel);
    }
}
