using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private static GameController instance = null;

    public static GameController Instance { get { return instance; } }

    [HideInInspector] public LevelManager LevelManager;
    [HideInInspector] public UIManager UIManager;
    [HideInInspector] public CameraController CameraController;
    [HideInInspector] public CharacterController CharacterController;
    [HideInInspector] public DataManager DataManager;
    [HideInInspector] public SpawnManager SpawnManager;
    [HideInInspector] public PoolManager PoolManager;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
