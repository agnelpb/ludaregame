using Collectables;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{   
    private Vector2 movementVector;
    private CharacterData characterData;

    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private SpriteRenderer characterSprite;
    [SerializeField] private Collider2D collider;

    private Vector3 spawnPosition;

    private enum CharacterState
    { 
        Walking,
        Jumping,
        DoubleJumping,
        Dead,
    }

    private CharacterState characterState;

    private void Awake()
    {
        spawnPosition = transform.position;

        GameController.Instance.CharacterController = this;
        StartCoroutine(WaitForServices());

        characterState = CharacterState.Walking;
    }

    public IEnumerator WaitForServices()
    {
        WaitForEndOfFrame frameWait = new WaitForEndOfFrame();
        while(GameController.Instance.DataManager == null)
        {
            yield return frameWait;
        }

        characterData = GameController.Instance.DataManager.CharacterData;
    }

    void Update()
    {
        movementVector.x = 0;

        if (characterState != CharacterState.Dead)
        {
            movementVector.x = Input.GetAxis("Horizontal");
            var newVelocity = rigidbody.velocity;
            newVelocity.x = (movementVector * characterData.WalkSpeed).x;
            rigidbody.velocity = newVelocity;

            if (Input.GetButtonDown("Jump"))            
            {
                if (characterState == CharacterState.Walking || characterState == CharacterState.Jumping)
                {                    
                    newVelocity = rigidbody.velocity;
                    newVelocity.y += characterData.JumpSpeed;
                    rigidbody.velocity = newVelocity;

                    characterState = characterState == CharacterState.Walking ? CharacterState.Jumping : CharacterState.DoubleJumping;
                    if (characterState == CharacterState.DoubleJumping)
                    {
                        StartCoroutine(WaitForEndOfDoubleJump((bool landed)=>
                        {
                            if (landed)
                            {
                                characterState = CharacterState.Walking;
                            }
                        }));
                    }
                }                
            }
        }
    }

    public IEnumerator WaitForEndOfDoubleJump(Action<bool> OnEndOfDoubleJump)
    { 
        WaitForEndOfFrame frameWait = new WaitForEndOfFrame();
        bool floorDetected = false;
                
        RaycastHit2D Hit;        
        while (!floorDetected)
        {
            Hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.down), 2f, layerMask);
            if (Hit.collider != null)
            {
                Debug.Log("Hitting " + Hit.transform?.tag);
                if (Hit.transform.tag == "Floor")
                {
                    floorDetected = true;
                }
            }

            if (characterState == CharacterState.Dead) { break; }
            yield return frameWait;
        }        
        OnEndOfDoubleJump?.Invoke(floorDetected);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (characterState == CharacterState.Dead) return;

        if (collision.transform.tag == "Danger")
        {
            OnCollisionWithDanger(collision);
        }
        else if (collision.transform.tag == "EndLevel")
        {
            OnCollisionWithEndLevel(collision);
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (characterState == CharacterState.Dead) return;

        if (collision.transform.tag == "Collectable")
        {
            OnCollisionWithCollectable(collision);
        }
    }

    private void OnCollisionWithDanger(Collision2D collision)
    {
        Kill();
    }

    private void OnCollisionWithEndLevel(Collision2D collision)
    {
        collision.gameObject.GetComponent<LevelEndObject>().EndLevel();
    }

    private void OnCollisionWithCollectable(Collider2D collision)
    {
        var collectable = collision.gameObject.GetComponent<Collectable>();
        var type = collectable.Type;
        var value = GameController.Instance.DataManager.CollectableData.GetValue(type);
        GameController.Instance.UIManager.IncrementCollectableCounter(value);
        collectable.OnCollected();
        
    }

    public void Kill()
    {
        Debug.Log("Killing Character");
        StartCoroutine(OnCharacterDeath());
    }

    private IEnumerator OnCharacterDeath()
    {
        characterState = CharacterState.Dead;

        var color = characterSprite.color;
        color.a = 0.5f;
        characterSprite.color = color;

        collider.enabled = false;
        rigidbody.Sleep();

        yield return new WaitForSeconds(1);

        WaitForEndOfFrame frameWait = new WaitForEndOfFrame();
        float distanceFromSpawn = Vector3.Distance(transform.position, spawnPosition);        
        while (distanceFromSpawn > 0.5f)
        {
            transform.position = Vector3.Lerp(transform.position, spawnPosition, Time.deltaTime * 2.0f);
            distanceFromSpawn = Vector3.Distance(transform.position, spawnPosition);            
            yield return frameWait;
        }

        color = characterSprite.color;
        color.a = 1.0f;
        characterSprite.color = color;

        collider.enabled = true;
        rigidbody.WakeUp();

        characterState = CharacterState.Walking;

        GameController.Instance.SpawnManager.SpawnAll();
        GameController.Instance.UIManager.ResetCounters();
    }
}
