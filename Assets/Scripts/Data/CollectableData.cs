using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Collectables.Collectable;

namespace Collectables
{
    [CreateAssetMenu(fileName = "CollectableData", menuName = "Data/CollectableData", order = 1)]
    public class CollectableData : ScriptableObject
    {
        [System.Serializable]
        public struct CollectableTypeData
        {
            public CollectableType Type;
            public int Value;
        }

        public List<CollectableTypeData> CollectableTypeDataList;

        private Dictionary<CollectableType, int> lookup = new Dictionary<CollectableType, int>();

        private void OnValidate()
        {
            PopulateLookupDictionary();
        }

        private void PopulateLookupDictionary()
        {
            foreach (var data in CollectableTypeDataList)
            {
                lookup[data.Type] = data.Value;
            }
        }

        public int GetValue(CollectableType type)
        {
            if (lookup.Count == 0)
            {
                PopulateLookupDictionary();
            }

            if (lookup.ContainsKey(type))
            {
                return lookup[type];
            }

            Debug.LogError("Please add the Value of " + type + " to Data/CollectableData");
            return 0;
        }
    }
}


