using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnemyAI;

[CreateAssetMenu(fileName = "EnemyData", menuName = "Data/EnemyData", order = 1)]
public class EnemyData : ScriptableObject
{
    [System.Serializable]
    public struct EnemyTypeData
    {
        public EnemyType EnemyType;
        public int Health;
        public float Speed;
    }

    public List<EnemyTypeData> EnemyTypeDataList;

    private Dictionary<EnemyType, EnemyTypeData> lookup = new Dictionary<EnemyType, EnemyTypeData>();

    private void OnValidate()
    {
        PopulateLookupDictionary();
    }

    private void PopulateLookupDictionary()
    {
        foreach (var data in EnemyTypeDataList)
        {
            lookup[data.EnemyType] = data;
        }
    }

    public EnemyTypeData GetData(EnemyType type)
    {
        if (lookup.Count == 0)
        {
            PopulateLookupDictionary();
        }

        if (lookup.ContainsKey(type))
        {
            return lookup[type];
        }

        Debug.LogError("Please add the data of " + type + " to Data/EnemyData");
        return new EnemyTypeData();
    }
}
