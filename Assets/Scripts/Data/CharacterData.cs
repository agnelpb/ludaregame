using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterData", menuName = "Data/CharacterData", order = 1)]
public class CharacterData : ScriptableObject
{
    public float WalkSpeed;
    public float JumpSpeed;
    public int MaxHealth;
    public int MaxLife;
}
