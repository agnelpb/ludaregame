using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text CollectablesCounter;
    public Text EnemyCounter;

    private int CollectableScore = 0;
    private int EnemyScore = 0;

    private void Awake()
    {
        GameController.Instance.UIManager = this; 
        ResetCounters();
    }

    public void IncrementCollectableCounter(int value)
    {
        CollectableScore+= value;
        UpdateUI();
    }

    public void IncrementEnemyScoreCounter()
    {
        EnemyScore++;
        UpdateUI();
    }

    public void ResetCounters()
    {
        CollectableScore = 0;
        EnemyScore = 0;
        UpdateUI();
    }

    public void UpdateUI()
    {
        CollectablesCounter.text = "x " + CollectableScore;
        EnemyCounter.text = "x " + EnemyScore;
    }
}
