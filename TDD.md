# Technical Design Document #

This is a technical design document for the Ludare 2D Platformer Prototype that will cover technical design and scheduling based on the GDD provided by Ludare.
This is meant to be a living document that will evolve over time. All estimates are to be considered simply as an estimate as opposed to fixed timeframes.

Note : This is an mini version of TDD. A full length TDD will contain details about architecture of various scripts and UML diagrams on how to design them.
That is out of scope for this tech test.

## About Game ##

The game is a 2D Platformer with a single level with basic game mechanics of a platformer.
Refer to GDD for game design.

## Game Mechanics & Features ##

### 1. Game Controller ###
*Class Type :  Singleton Monobehaviour*

Game Controller manages the core loop and persist across the life cycle of the game.

  + Core Loop
  + Access to various services and managers.

### 2. Character Controller ###
*Class Type :  Monobehaviour*

Character controller is attached to the player character.

  + Implements character state.
  + Listens for player input
  + Updates animation controller based on character state
  + Checks for collision with collectables, AI and end level condition.

### 3. Camera Controller ###
*Class Type :  Monobehaviour*

Camera controller tracks the character controller across the level. Also used by UI Manager to display UI.

  + Player tracking

### 4. Collectable ###
*Class Type :  Base Class, Monobehaviour*

Collectable is a base class implementation for all collectables in the game.
Advanced Implementation : Collectables generated using data & stats provided in the tuning tool.
This is beyond the scope of the initial TDD, but something to keep in mind.

  + Coin pickup

### 5. Enemy AI ###
*Class Type :  Base Class, Monobehaviour*

EnemyAI is a base class implementation for all enemies in the game.
Advanced Implementation : Different enemy types generated using data & stats provided in the tuning tool.

  + Moving Enemy ( Inherited class & movement implementation)  

### 6. End Level Object ###
*Class Type :  Monobehaviour*

EndLevelObject is attached to the object that end the level.

### 7. UI Manager ###
*Class Type :  Monobehaviour*

UI Manager updates the score, lives and menu for the player.

  + Score UI
  + Lives Left UI
  + Main Menu UI
  + Score tracking
  + Lives tracking

### 9. Pooling System ###
*Class Type :  Monobehaviour*

Generic Pooling System that is used to increase performance of the game.

  + Generic Pool Container.
  + Pool Manager

Note : A existing plugin written by Agnel Blaise will be used for this implementation.

### 10. Data System / Tuning Tooling ###
*Class Type : Scriptable Objects, Monobehaviour*

A data system can be implemented initially with Scriptable Objects to make it easy for designers and artists
to tweak the game parameters. The Data system will be accessed by the Game Controller to set up the game.

Advanced Implementation : Tuning Tooling can be implemented using plugins available on Unity Asset store to
access a password-protected spreadsheet and data can be read from cells to override the data in the scriptable objects.
In the absence of an internet connection, the game will fallback to use the data provided by scriptable objects.

  + Data Manager.
  + Data Scriptable Objects for Enemy stats, Collectable Stats etc

### 11. Level Manager ###
*Class Type : Monobehaviour*

Level loading system will be used to load different level scenes. Uses default Unity level loading.

  + Level Loading


## Task & Time Estimates ##

### Programming Tasks ###

Note : For Programming, Priority is high for components that is essential to core loop, Mid for gameplay elements and Low for any optimization / dev tools.
For Art, Priority is High for gameplay elements.

#### Assignee : Agnel Blaise ####

- Game Controller  

    + Priority : High
    + Estimate : 2 hour

- Character controller

    + Priority : High
    + Estimate : 2 hours

- Camera Controller

    + Priority : High
    + Estimate : 1 hours

- Collectables

    + Priority : Mid
    + Estimate : 30 mins.

- EnemyAI

    + Priority : Mid  
    + Estimate : 45 mins

- End Level Objects

    + Priority : Mid
    + Estimate : 30 mins

- UI Manager

    + Priority : High / Mid
    + Estimate : 2 hours

- Pooling system ( Plugin Integration )

    + Priority : Low
    + Estimate : 15 mins

- Data System

    + Priority : Mid
    + Estimate : 30 mins.

- Level Manager

    + Priority : High / Mid
    + Estimate : 30 mins    

- Deadly Object

    + Priority : Mid
    + Estimate : 30 mins       

- Bug Fixing ( Buffer time for devs to clean up issues )

    + Priority : Mid / Low
    + Estimate : 2 hours

### Art Tasks ###

#### Assignee : Agnel Blaise ####

- Character Live / Ghost Form animator

    + Priority : High
    + Estimate : 30 mins

- Character Sprite

    + Priority : High
    + Estimate : 5 mins

- Enemy Sprite

    + Priority : High
    + Estimate : 5 mins

- Level Design Sprites

    + Priority : High
    + Estimate : 5 mins

- Gameplay UI Sprites

    + Priority : High
    + Estimate : 5 mins

### Design Tasks ###

#### Assignee : Agnel Blaise ####

- Level Design

    + Priority : High
    + Estimate : 1 hour

- Menu Design

    + Priority : High
    + Estimate : 30 mins

### Milestones ###

1. Game Loop

    + Estimate : 6 hours

2. Gameplay Feature Implementation

    + Estimate : 4 - 5 hours

3. Prototype

    + Estimate : 2 - 3 hours


## Standards & Nomenclature ##

### Programming ###

The programmers will be following the Coding standards set by Microsoft for C#.

Ref :  https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions

### Art & Design ###

Ref : https://docs.unity3d.com/2020.1/Documentation/Manual/HOWTO-ArtAssetBestPracticeGuide.html

## Tools ##

* Unity 2020.3.19f1 LTS.
* Adobe Photoshop 2020
* SourceTree
* Visual Studio Community 2019

## Copyright Information ##

Copyright 2021 Agnel P Blaise

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
